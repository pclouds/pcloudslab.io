#!/usr/bin/ruby

require 'open-uri'

def get_friends(you, no_fetch = nil)
  if File.exists? "friends/#{you}"
    friends = File.readlines("friends/#{you}").map {|x| x.chomp}
  else
    return [] unless no_fetch.nil?
    puts "Getting http://360.yahoo.com/friends-#{you}"
    if File.exists? "www/friends-#{you}"
      html = open("www/friends-#{you}").read
    else
      begin
        html = open("http://360.yahoo.com/friends-#{you}").read
      rescue EOFError
        puts "Ignore #{you}"
        html = ""
      end
      html_cache = File.open("www/friends-#{you}", "w")
      html_cache.write html
      html_cache.close
    end
    ids = html.grep(/360.yahoo.com\/profile-([^"?]*)/) { |obj| $1; }.sort.uniq.find_all {|x| x != you}
    out = open("friends/#{you}", "w")
    ids.each {|x| out.puts x;}
    out.close
    ids
  end
end

def crawl_friends(you, maxdepth=1)
  todo_list = [[0,you]]
  in_list = {you => true}
  counter = 0
  while !todo_list.empty?
    depth, todo = todo_list.shift
    counter = counter + 1
    puts "#{todo}(#{depth}) #{counter}/#{todo_list.length}"
    friends = get_friends(todo)
    for f in friends
      next if depth >= maxdepth
      unless in_list.key? f
        todo_list << [depth+1,f]
        in_list[f] = true
      end
    end
  end
end

def collect_names(you)
  if File.exists? "www/friends-#{you}"
    html = open("www/friends-#{you}").read
    names = {}
    html.grep(/360.yahoo.com\/profile-([^"?]*).*title="([^"]+)"/) do |obj|
      names[$1] = $2 if names[$1].nil? || names[$1].empty?
    end
    names
  else
    {}
  end
end

def get_name(you,your_friend)
  names = collect_names(you)
  names.key? your_friend? ? names[your_friend] : nil
end

def to_dot(you,maxdepth = 1)
  done_list = []
  todo_list = []
  if you.is_a? String
    todo_list << [0,you]
  else
    you.each {|x,y| todo_list << [0,y]}
  end
  in_list = {}
  if you.is_a? String
    in_list[you] = [0, "Me", {"color" => "red"},0]
    counter = 1
  else
    counter = 0
    you.each {|x,y| in_list[y] = [counter,x, {"color" => "red"},0];counter = counter + 1}
  end
  edges = {}

  puts "graph a {"
  puts "overlap=\"scalexy\";"
  puts "node [fontsize=6,height=0.1];"


  while !todo_list.empty?
    depth, todo = todo_list.shift
    done_list << todo
    STDERR.puts "#{todo[0..20]}/#{depth} #{done_list.length}/#{todo_list.length}"
    friends = get_friends(todo,true)
    friend_names = collect_names(todo)
    for f in friends
      next unless File.exists? "friends/#{f}"
      next if depth >= maxdepth and !in_list.key? f

      unless in_list.key? f
        todo_list << [depth+1,f] 
        in_list[f] = [counter, friend_names[f] || counter, {}, 1]
        counter = counter + 1
      end

      if f != todo
        #puts "\"#{name[todo]}\" -> \"#{name[f]}\";"
        edge = in_list[todo][0] < in_list[f][0] ? [todo,f] : [f,todo]
	unless edges.key? edge
        att = {}
        if in_list[todo][3] == 0 or in_list[f][3] == 0
          att["color"] = "red"
	  if in_list[todo][3] == 0
	    in_list[f][3] += 1
	  else
	    in_list[todo][3] += 1
	  end
	end
        if (in_list[todo][3] >= 2 and in_list[f][3] == 1) or (in_list[todo][3] == 1 and in_list[f][3] >= 2)
          att["color"] = "blue"
	end
        edges[edge] = att unless edges.key? edge
	end
      end
    end
  end
  for k,v in in_list
    att = v[2] || {}
    att["label"] = v[1]
    att["color"] = "blue" if v[3] == 2 and maxdepth > 1
    att["color"] = "green" if v[3] > 2
    att["color"] = "yellow" unless att.key? "color"
    attstr = "["+att.map{|x,y| "#{x}=\"#{y}\""}.join(",")+"]"
    puts "\"#{v[0]}\" #{attstr};"
  end
  for k,v in edges
    attstr = ''
    attstr = "["+v.map{|x,y| "#{x}=\"#{y}\""}.join(",")+"]" unless v.empty?
    puts "\"#{in_list[k[0]][0]}\" -- \"#{in_list[k[1]][0]}\" #{attstr};"
  end
  puts "};"
end

if ARGV[0] == "crawl"
  if ARGV.length == 3
    crawl_friends ARGV[1], ARGV[2].to_i
  else
    crawl_friends ARGV[1]
  end
end
if ARGV[0] == "dot"
  if ARGV.length == 2
    to_dot ARGV[1]
  else
    you = Hash[*ARGV[1..-1]]
    to_dot(you)
  end
end
